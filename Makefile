.PHONY: build run

IMAGE_TAG?=dev
IMAGE_NAME="nginx-no-ssl-proxy"

build:
	docker build \
		-t $(IMAGE_NAME):$(IMAGE_TAG) \
	.

run:
	docker run \
		-p 8080:80 \
		$(IMAGE_NAME):$(IMAGE_TAG)
		
run-it:
	docker run \
		-p 8080:80 \
		-it \
		$(IMAGE_NAME):$(IMAGE_TAG) \
		bash