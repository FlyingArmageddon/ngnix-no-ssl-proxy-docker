Simple no-ssl proxy

Sources:
- https://superuser.com/questions/1487553/proxy-or-other-solution-that-can-allow-vintage-browsers-before-https-era-wi
- https://serverfault.com/questions/713148/modify-html-pages-returned-by-nginx-reverse-proxy
- http://nginx.org/en/docs/http/ngx_http_sub_module.html